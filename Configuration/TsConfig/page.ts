// try a workaround to prevent sys_file records being stored on uid=0
TCAdefaults.sys_file {
  pid = 1
}
options.clearCache.system=1
options.clearCache.pages = 1
options.clearCache.all = 1
