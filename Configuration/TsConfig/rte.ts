RTE {
	default {
		FE >
		buttons {
		  formatblock {
		    remoteItems = h3,h4,h5,h6,pre,address,blockquote,div,article,aside,container,footer,header,headline1,headline2,
		  }
		}
    hideButtons = chMode, formatblock, textstylelabel, textstyle, fontstyle, fontsize, lefttoright, righttoleft, language, showlanguagemarks, formattext, bidioverride, big, citation, code, definition, deletedtext, insertedtext, keyboard, monospaced, quotation, sample, small, span, strikethrough, strong, subscript, superscript, variable, textcolor, bgcolor, textindicator, editelement, showmicrodata, emoticon, insertcharacter, insertsofthyphen, line, image, table, tableproperties, tablerestyle, rowproperties, rowinsertabove, rowinsertunder, rowdelete, rowsplit, columnproperties, columninsertbefore, columninsertafter, columndelete, columnsplit, cellproperties, cellinsertbefore, cellinsertafter, celldelete, cellsplit, cellmerge
    proc {
      overruleMode = ts_css
      dontUndoHSC_db = 1
      dontHSC_rte = 1
      entryHTMLparser_db = 1
      entryHTMLparser_db {
        p {
          fixAttrib {
            style = unset
          }
        }
        div {
          fixAttrib {
            style = unset
          }
        }
      }
    }
	}
  config {
    tt_content {
      bodytext {

      }
    }
  }
}
