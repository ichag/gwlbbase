config {
  # Enable Links Across Domains: Post TYPO3 4.2
  typolinkEnableLinksAcrossDomains = 1

   # Page Title First:If set (and the page title is printed) then the page-title will be printed BEFORE the template title.
  pageTitleFirst = 1

   # Redirect Old Links to New
  redirectOldLinksToNew = 1

   # Correct Multiple Domain Links
  typolinkCheckRootline = 1

   # Disable border attribute of img tags
  disableImgBorderAttr = 1

   # Disable Base Extension Wrap
  disableBaseWrap = 1

   # Disable Extension BEGIN/END in HTML
  disablePrefixComment = 1

   # Send Cache Control Headers:Output cache-control headers to the client
  sendCacheHeaders = 1

   # Minify CSS: CSS files will be minified and compressed. For TYPO3 4.5+
  compressCss = 0

   # Merge CSS &amp; JS: Merges Stylesheet and JavaScript files referenced in the Frontend together. For TYPO3 4.5+
  concatenateJsAndCss = 0

   # Remove Default CSS: Default CSS in the header will be removed.
  removeDefaultCss = 0

   # Minify JS: JS files will be minified and compressed. For TYPO3 4.5+
  compressJs = 0

   # Remove Default JS: Default JavaScript in the header will be removed.
  removeDefaultJS = 1

   # Link to TYPO3 CSS:If set, the inline styles TYPO3 controls in the core are written to a file.
  inlineStyle2TempFile = 1

   # Unique Link Vars: Prevent links with the same parameter more than once.
  uniqueLinkVars = 1

}
