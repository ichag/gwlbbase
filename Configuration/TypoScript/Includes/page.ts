page = PAGE
page {
  config {
    admPanel = 1
    tx_realurl_enable = 1
    absRefPrefix = /
    doctype = html5
    htmlTag_langKey = de-DE
    index_enable = 1
    index_externals = 1
  }
  includeJS {
    10-jquery = //ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
  }
  includeCSS {
    10-bootstrap-select = EXT:gwlbbase/Resources/Public/Stylesheets/vendor/bootstrap-select.min.css
    20-bootstrap-select-picker = EXT:gwlbbase/Resources/Public/Stylesheets/vendor/selectpicker.css
    30-layout = EXT:gwlbbase/Resources/Public/Stylesheets/layout.css
    40-fonts = //fonts.googleapis.com/css?family=Lato:400,700
  }
  10 = FLUIDTEMPLATE
  10 {
    file = EXT:gwlbbase/Resources/Private/Templates/Page/Index.html
    templateRootPath = EXT:gwlbbase/Resources/Private/Templates/
    partialRootPath = EXT:gwlbbase/Resources/Private/Partials/
    layoutRootPath = EXT:gwlbbase/Resources/Private/Layouts/
    variables {
      left < styles.content.getLeft
      left.slide = -1
      center < styles.content.get
      right < styles.content.getRight
      right.slide = -1
      navMain =< lib.navMain
      breadcrumb =< lib.breadcrumb
      providers =< lib.providers
      providers.slide = -1
    }
  }
}
