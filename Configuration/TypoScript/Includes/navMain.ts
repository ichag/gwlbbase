lib.navMain = HMENU
lib.navMain {
  entryLevel = 0
  1 = TMENU
  1.wrap = <div>|</div>
  1{
    expAll = 1
    NO{
      wrapItemAndSub = <p>|</p>
      stdWrap.cObject = COA
      stdWrap.cObject {
        10 = TEXT
        10.field = title
        10.wrap = <span>|</span>
        20 = IMAGE
        20.file {
          import = uploads/media/
          import.field = media
          import.listNum = 0
        }
      }
    }
    ACT = 1
    ACT{
      wrapItemAndSub = <p>|</p>
      stdWrap.cObject = COA
      stdWrap.cObject {
        10 = TEXT
        10.field = title
        10.wrap = <span>|</span>
        20 = IMAGE
        20.file {
          import = uploads/media/
          import.field = media
          import.listNum = 0
        }
      }
      ATagParams = class="current"
    }
    CUR = 1
    CUR{
      wrapItemAndSub = <p class="current">|</p>
      ATagParams = class="current"
      stdWrap.cObject = COA
      stdWrap.cObject {
        10 = TEXT
        10.field = title
        10.wrap = <span>|</span>
        20 = IMAGE
        20.file {
          import = uploads/media/
          import.field = media
          import.listNum = 0
        }
      }
    }
  }
}
