##########################################
# Auswahl sprachen
##########################################
# SprachVariable L initialisieren
config.linkVars = L
config.sys_language_uid = 0
config.language = en
config.locale_all = en_EN.UTF-8

#Auch wenn eine Seite nicht übersetzt ist, bleibt man auf der Webseite in der gewählten Sprache (Menüs sind übersetzt etc..) und es wird stattdessen der Inhalt der Defaultsprache angezeigt.
config.sys_language_mode = content_fallback
#Fehlermeldung falls nicht übersetzte Seiten angezeigt werden sollen
#config.sys_language_mode=strict

#Nur übersetzte Elemente oder welche mit Ländereinstellung "all" werden angezeigt.
#config.sys_language_overlay = hideNonTranslated
#Alle Elemente werden angezeigt (egal welche Spracheinstellung), falls eine Übersetzung existiert, wird diese angezeigt.
config.sys_language_overlay = 1

#Standardsprache UID 0 -> Deutsch
