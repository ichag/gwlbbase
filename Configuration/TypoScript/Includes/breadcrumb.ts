# Breadcrumb-Funktion (Pfadanzeige, rootline)
lib.breadcrumb = HMENU
lib.breadcrumb {
  special = rootline
  special.range = 0|3
  entryLevel = 0

  1 = TMENU
  1 {
    noBlur = 1
    NO {
      allWrap =   |   >    |*|  |   >   |*| |
      stdWrap.htmlSpecialChars = 1
    }
    CUR = 1
    CUR {
      doNotLinkIt = 1
    }
   }
   2 < 1
   2 {

   }
}
