<?php
namespace Datenbetrieb\Holzherbase\UserFunc;
class PageNotFound {

    /**
     * Redirect to 404 error page with language-ID provided by ext. "realurl_force404lang" (if installed)
     * Redirect to login page
     *
     * @param array  $params  : "currentUrl", "reasonText" and "pageAccessFailureReasons"
     * @param object $tsfeObj : object type "tslib_fe"
     */
    public function pageNotFound(&$params, &$pObj) {
        // if try to access restricted page
        if (array_shift($params['pageAccessFailureReasons']['fe_group'])) {
            // build the base url and append lang switch and requested URL
            $url = \TYPO3\CMS\Core\Utility\GeneralUtility::getIndpEnv('TYPO3_REQUEST_HOST') . '/';
            $url .= '?L=' . (array_key_exists('L', $_GET) ? $_GET['L'] : '0');
            $url .= '&redirect_url=' . urlencode($params['currentUrl']);
            \TYPO3\CMS\Core\Utility\HttpUtility::redirect($url, \TYPO3\CMS\Core\Utility\HttpUtility::HTTP_STATUS_401);
            // default 404 handling. go to a site with url alias "notfound" and lang switch. if the 404 page not exists throw 404 error.
        } else {
            $url = '/index.php?id=notfound&L=' . (array_key_exists('L', $_GET) ? $_GET['L'] : '0');
            $pObj->pageErrorHandler($url);

        }
    }
}

?>
